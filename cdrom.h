/* cdrom.h
Copyright (c) 2000-2012 Craig Condit, Stefan Huelswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#ifndef _CDBACKUP_CDROM_H
#define _CDBACKUP_CDROM_H

#ifdef linux
#include <linux/cdrom.h>
#endif

#ifdef sun
#include <sys/cdio.h>
#endif

#ifndef CD_FRAMESIZE
#define CD_FRAMESIZE 2048
#endif

struct cd_header {
  int start_track, end_track;
  int used;
  };

struct cd_track {
  int start_sec;
  int leadout_size;
  int is_data;
  int start_track;
  };

void get_param(int fd, unsigned long *ahead, unsigned long *fahead);
void set_param(int fd, unsigned long ahead, unsigned long fahead);
int getCdHeader(struct cd_header *cd);
void getCdTrack(int num, struct cd_track *cd);

#endif
