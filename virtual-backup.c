/* virtual-backup.c
Copyright (c) 2000-2012 Craig Condit, Stefan H�lswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#define _LARGEFILE64_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>

#include "virtual.h"
#include "cdbackup.h"
#include "cdrom.h"
#include "misc.h"
#include "debug.h"

//#define DEBUGOUT

extern int virtual, multi;

extern int fd;
extern struct virt_header *virt_header;
extern int virtualMissing;
extern char *real_virt_name;

#ifndef DEBUGOUT
static pid_t childpid;
#endif
static int vfd=-1;

/****************************************************************************/

void VnewTrack(void)
{
  if(virtual) {
    long long pos;
    if(virtualMissing) {
      if((fd=open64(real_virt_name,O_RDWR|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP| S_IROTH))<0) error("Open failed (new track)");
      if(write(fd,virt_header,CD_FRAMESIZE)!=CD_FRAMESIZE) error("Write failed (new track)");
      virtualMissing=0;
      }
    if(virt_header->tracks>=MAX_VIRT_TRACKS-1) serror("Maximum number of virtual tracks reached");
    pos=(long long)(virt_header->start[virt_header->tracks]=virt_header->leadout)*CD_FRAMESIZE;
    virt_header->tracks++;
    if(lseek64(fd,pos,SEEK_SET)<0) error("Seek failed (new track)");
    }
  else {
#ifndef DEBUGOUT
    int pd[2];
    Vclose();
    if(pipe(pd)<0) error("Pipe failed (new track)");
    if((childpid=fork())<0) error("Fork failed (new track)");
    if(childpid==0) { /* child */
      close(pd[1]);
      close(0);
      dup2(pd[0],0);
      start_cdrecord(); /* doesn't returns */
      }
    close(pd[0]); fd=pd[1];
#else
    /* debug code; send data to /dev/null. */
    Vclose();
    fprintf(stderr,"DEBUG CODE: NO recording, sending data to /dev/null!\n");
    if((fd=open("/dev/null",O_WRONLY))<0) error("Open failed (/dev/null)");
#endif
    cd_avail-=padsize*CD_FRAMESIZE;
    }
}

/****************************************************************************/

int Vsize(void)
{
  return virt_header->leadout;
}

/****************************************************************************/

int VhasCont(void)
{
  return virt_header->has_cont;
}

/****************************************************************************/

void VprepareDump(void)
{
  if((vfd=dup(fd))<0) error("Dup failed");
  Vclose();
}

/****************************************************************************/

void VvirtRead(void *buf)
{
  if(full_read(vfd,buf,CD_FRAMESIZE)!=CD_FRAMESIZE)
    serror("Unexpected EOF reading data");
}

/****************************************************************************/

void VcloseTrack(int cont)
{
  if(virtual) {
    if(lseek64(fd,0,SEEK_SET)<0) error("Seek failed (close track)");
    virt_header->has_cont=cont;
    if(write(fd,virt_header,CD_FRAMESIZE)!=CD_FRAMESIZE) error("Write failed (close track)");
    Vclose();
    }
  else {
    Vclose();
#ifndef DEBUGOUT
    DEBUG("VcloseTrack: waiting for child termination\n");
    while(wait(0)!=childpid);
#endif
    }
  if(vfd>=0) { close(vfd); vfd=-1; }
}

/****************************************************************************/

void Vwrite(const void *buf)
{
  if(write(fd,buf,CD_FRAMESIZE)!=CD_FRAMESIZE) error("Write failed");
  if(virtual) virt_header->leadout++;
  cd_avail-=CD_FRAMESIZE;
}
