# compiler selection and options
CC       = gcc
CFLAGS   = -g -O2 -Wall
DEPFLAGS = -MM -MG

# install location
PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/man

###############################################

TARGETS = cdbackup cdrestore
VERSION = $(shell grep 'define VERSION' version.h | awk '{ print $$3 }' | sed -e 's/["]//g')
ARCHIVE = cdbackup-$(VERSION)
DEPFILE = .dependencies
TMPDIR  = /tmp

all: $(TARGETS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(DEPFILE): Makefile
	@$(CC) $(DEPFLAGS) cdbackup.c cdrestore.c cdrom.c virtual.c misc.c > $@

include $(DEPFILE)

cdbackup: cdbackup.o cdrom.o virtual.o virtual-backup.o misc.o
	$(CC) $(CFLAGS) -o $@ $^

cdrestore: cdrestore.o cdrom.o virtual.o misc.o
	$(CC) $(CFLAGS) -o $@ $^

strip: $(TARGETS)
	strip $(TARGETS)

clean:
	rm -f $(TARGETS) $(DEPFILE) cdbackup-*.tar.gz core *.core *.o

dist: clean
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@mkdir $(TMPDIR)/$(ARCHIVE)
	@cp -a * $(TMPDIR)/$(ARCHIVE)
	@tar czf $(ARCHIVE).tar.gz -C $(TMPDIR) $(ARCHIVE)
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@echo Distribution package created as $(ARCHIVE).tar.gz

install:
	install -d $(BINDIR)
	install $(TARGETS) $(BINDIR)
	install -d $(MANDIR)/man1
	gzip -c cdbackup.1 >$(MANDIR)/man1/cdbackup.1.gz
	gzip -c cdrestore.1 >$(MANDIR)/man1/cdrestore.1.gz
