/* cdbackup.h.
Copyright (c) 2000-2012 Craig Condit, Stefan Huelswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#ifndef _CDBACKUP_H
#define _CDBACKUP_H

#include "version.h"

#define NO_LABEL	"CD-Backup Track"
#define SHORT_HDR	"cdbackup"
#define HDR_STRING	SHORT_HDR" "VERSION

#define MIN_BLOCKS	2   /* min. required blocks for a session: header block, one data block */

#define F_NONE   0   /* flag values */
#define F_CRC    1   /* backup has CRC checksum */

struct header_block {
  char id_str[33];	/* recorder id string (32 chars) */
  char vol_id[33];	/* volume label (32 characters) */
  char t_stamp[13];	/* timestamp */
  char disk_set;	/* disk number - starts with 1 on multi-disk set */
  char flags;           /* flags for the backup set */
};

struct data_block {
  char status;		/* status of block (0=continue, 1=done, 2=disk full) */
  char flags;		/* flags for this data block */
  short datasize;	/* # of bytes in block (max = CD_FRAMESIZE-this) */
};

extern int padsize;
extern long cd_len;
extern int verbose;
extern char *prg_name;

void start_cdrecord(void);

#define DBSIZE   sizeof(struct data_block)
#define DATASIZE (CD_FRAMESIZE-DBSIZE)

#endif
