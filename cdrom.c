/* cdrom.c
Copyright (c) 2000-2012 Craig Condit, Stefan Huelswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#define _LARGEFILE64_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>

#ifdef linux
#include <linux/fs.h>
#endif

#include "cdrom.h"
#include "misc.h"
#include "debug.h"

/* size of leadin/out depending of how many tracks are on cd */
#define LEADOUT_1	11400 /* 1th track */
#define LEADOUT_2	6900  /* 2nd and more tracks */
/* number of (unreadable) runout sectos */
#define RUNOUT          2

extern int fd;
extern int verbose;
extern char *prg_name;

/****************************************************************************/

void get_param(int fd, unsigned long *ahead, unsigned long *fahead)
{
#if defined(BLKRAGET) && defined(BLKFRAGET)
  *ahead = *fahead = 0;
  ioctl(fd,BLKRAGET,ahead);
  ioctl(fd,BLKFRAGET,fahead);
  DEBUG("get_param: readahead=%ld freadahead=%ld\n",*ahead,*fahead);
#else
  fprintf(stderr,"Can't get readahead parameter. Ioctl's not available\n");
#endif
}

/****************************************************************************/

void set_param(int fd, unsigned long ahead, unsigned long fahead)
{
#if defined(BLKRAGET) && defined(BLKFRAGET)
  ioctl(fd,BLKRASET,ahead);
  ioctl(fd,BLKFRASET,fahead);
  DEBUG("set_param: readahead=%ld freadahead=%ld\n",ahead,fahead);
#else
  fprintf(stderr,"Can't set readahead parameter. Ioctl's not available\n");
#endif
}

/****************************************************************************/

int getCdHeader(struct cd_header *cd)
{
  struct cdrom_tochdr cd_header;
  struct cdrom_tocentry cd_entry;
  int tracks;

  if(ioctl(fd,CDROMREADTOCHDR,&cd_header)<0) {
    if(verbose) fprintf(stderr,"%s: Unable to read CD header, assuming empty CD-R\n",prg_name);
    cd->start_track=1; cd->end_track=0;
    cd->used=0;
    return 0;
    }

  cd->start_track=cd_header.cdth_trk0;
  cd->end_track  =cd_header.cdth_trk1;
  tracks=cd->end_track-cd->start_track+1;

  cd_entry.cdte_format=CDROM_LBA;
  cd_entry.cdte_track=CDROM_LEADOUT;
  if(ioctl(fd,CDROMREADTOCENTRY,&cd_entry)<0) error("Ioctl failed (lead-out)");

  cd->used=cd_entry.cdte_addr.lba + (tracks==1 ? LEADOUT_1:LEADOUT_2);
  return tracks;
}

/****************************************************************************/

void getCdTrack(int num, struct cd_track *cd)
{
  struct cdrom_tocentry cd_entry;

  cd_entry.cdte_format=CDROM_LBA;
  cd_entry.cdte_track=num;
  if(ioctl(fd,CDROMREADTOCENTRY,&cd_entry)<0) error("Ioctl failed (toc entry)");  

  cd->start_sec   =cd_entry.cdte_addr.lba;
  cd->leadout_size=RUNOUT+(num==cd->start_track ? LEADOUT_1:LEADOUT_2);
  cd->is_data     =(cd_entry.cdte_ctrl&CDROM_DATA_TRACK) ? 1:0;
}
