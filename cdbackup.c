/* cdbackup.c
Copyright (c) 2000-2012 Craig Condit, Stefan Huelswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#define _LARGEFILE64_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <netinet/in.h>

#ifndef sun
#include <getopt.h>
#endif

#include "cdbackup.h"
#include "cdrom.h"
#include "virtual.h"
#include "misc.h"
#include "debug.h"
#include "version.h"

/* defaults */
char *prg_name ="cdbackup";
char *cd_dev   ="/dev/burner";
char *cdr_dev  =0;                     /* no default here, too dangerous */
char *cd_label ="CDBackup Track";
int   cd_speed =4;
long  cd_len   =-1;                    /* blocks */
int   padsize  =15;                    /* blocks */
int   multidisk=0;
char *multicmd =0;
int   verbose  =0;
int   xamode2  =0;
int   crc      =1;
int   debug    =0;
int   virtual  =0;
char *virt_name=0;
int   virt_dump=0;
int   dvd      =0;
char *exename  ="cdrecord";

char **cdrec_opt=0;
int    cdrec_opt_count=0;

long long totalSize=0;
int disknum=1;
int secs;
struct tm curtime;  /* global, so multi-disks get all the same time */
int auto_size=0;

/****************************************************************************/

char *make_arg(const char *format, ...)
{
  char *ptr;
  va_list ap;
  va_start(ap,format);
  if(vasprintf(&ptr,format,ap)<0) {
    serror("No memory for cdrecord args\n");
    ptr=0;
    }
  va_end(ap);
  return ptr;
}

void start_cdrecord(void)
{
  char **args, **p;
  int l;
  
  if(!(p=args=calloc(cdrec_opt_count+32,sizeof(char *))))
    serror("No memory for cdrecord args\n");

  *p++=exename;

  if(virt_dump || dvd) {
    *p++="-dao";
    *p++=make_arg("tsize=%ds",secs);
    }
  else {
    *p++="-multi";
    *p++="-tao";
    *p++=make_arg("padsize=%ds",padsize);
    }

  *p++=make_arg("speed=%d",cd_speed);
  *p++=make_arg("dev=%s",cdr_dev);

  for(l=0 ; l<cdrec_opt_count ; l++) *p++=cdrec_opt[l];

  if(xamode2 && !dvd) *p++="-xa2"; else *p++="-data";
  *p++="-";
  *p++=0;

  if(debug) {
    fprintf(stderr,"%s: cdrecord command:",prg_name);
    for(p=args ; *p ; p++) fprintf(stderr," %s",*p);
    fprintf(stderr,"\n");
    }

  execvp(exename,args);
  error("Exec failed (cdrecord)");
}

long atip_cdrecord(void)
{
  char *cmd;
  FILE *p;
  long size=-1;
  
  if(asprintf(&cmd,"%s 2>&1 dev=%s -atip",exename,cdr_dev)<0) {
    fprintf(stderr,"%s: error making atip command: %s\n",prg_name,strerror(errno));
    return -1;
    }
  DEBUG("%s: cdrecord atip command: %s\n",prg_name,cmd);

  p=popen(cmd,"r");
  if(!p) fprintf(stderr,"%s: atip command failed\n",prg_name);
  else {
    char buff[256];
    while(fgets(buff,sizeof(buff),p)) {
      if(dvd) {
         /* DVD-R */
	 if(!strncmp(buff,"rzone size:",11)) size=strtol(&buff[11],NULL,10);
	 /* DVD+R */
	 else if(!strncmp(buff,"phys size:...",13)) size=strtol(&buff[13],NULL,10);
	 }
      else if(!strncmp(buff,"  ATIP start of lead out:",25)) size=strtol(&buff[25],NULL,10);
      }
    }
  pclose(p);
  free(cmd);
  if(size>0 && verbose) {
    char buff[16];
    fprintf(stderr,"%s: auto-detected media size %s (%ld blocks)\n",prg_name,FlexSize(buff,(long long)size*CD_FRAMESIZE),size);
    }
  return size;
}

/****************************************************************************/

void parse_cmdline(char argc, char *argv[]) 
{
  int i;
  char *val;

  /* get some default from the environment */
  val=getenv("CDR_DEVICE");
  if(val) {
    cdr_dev=strdup(val);
    DEBUG("cdbackup: using recording device %s from CDR_DEVICE\n",cdr_dev);
    }
  val=getenv("CDR_SPEED");
  if(val) {
    cd_speed=strtol(val,NULL,10);
    DEBUG("cdbackup: using speed %d from CDR_SPEED\n",cd_speed);
    }
  
  while ((i=getopt(argc,argv,"d:r:l:s:p:a:c:mvVXDCi:wRE:"))>0) {
    switch (i) {
       case 'V': fprintf(stderr,"cdbackup "VERSION" (compiled "__DATE__")\n"
	                        "Copyright (C) 2000-2010\n"
			        "This is free software; see the source for copying conditions.\n"
			        "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
			        "PARTICULAR PURPOSE.\n");
                 exit(0);
       case 'v': verbose=1; break;
       case 'm': multidisk=1; break;
       case 'X': xamode2=1; break;
       case 'c': multicmd=optarg; break;
       case 'd': cd_dev=optarg; break;
       case 'r': cdr_dev=optarg; break;
       case 'a': cd_label=optarg; break;
       case 'C': crc=0; break;
       case 'i': virt_name=optarg; virtual=1; break;
       case 'w': virt_dump=1; break;
       case 'E': exename=optarg; break;
       case 'R': dvd=1;
                 DEBUG("cdbackup: DVD mode enabled\n");
                 break;
       case 'D': verbose=1; debug=1; 
                 DEBUG("cdbackup: DEBUG output enabled ("VERSION")\n");
                 break;
       case 'l': cd_len=(long)(FlexLen(optarg)/CD_FRAMESIZE);
                 break;
       case 's': errno=0; cd_speed=strtol(optarg,NULL,10);
                 if(errno==ERANGE || cd_speed<1) serror("Option -s: speed out of range (must be >=1)\n");
	         break;
       case 'p': errno=0; padsize=strtol(optarg,NULL,10);
                 if(errno==ERANGE || padsize<15) serror("Option -p: padsize out of range (must be >=15)\n");
	         break;
       default:  fprintf(stderr,
                         "Usage: %s [options ...] [-- cdrecord-options ...]\n"
                         "Reads from standard input, block formats and writes to CD-R(W).\n\n"
                         "  -d DEVICE      DEVICE for CD queries (default /dev/burner)\n"
                         "  -l N           set media size, disable auto-detect\n"
                         "  -r DEVICE      DEVICE for CD recording (e.g. 0,4,0)\n"
                         "  -s N           record CD at speed N (default 4)\n"
                         "  -X             enable CDROM XA2 mode in cdrecord\n"
                         "  -a LABEL       use LABEL as CD session title\n"
                         "  -p N           use a padsize of N sectors for the session (default 15)\n"
                         "  -m             enable multi-disk mode\n"
                         "  -c COMMAND     call COMMAND on disk change in multi-disk mode\n"
                         "  -C             disable checksum creation for datablocks\n"
                         "  -i IMAGE       use virtual image IMAGE for recording\n"
                         "  -w             dump virtual image to media\n"
                         "  -R             enables DVD mode\n"
                         "  -E EXE         set alternative cdrecord executable\n"
                         "  -v             be verbose\n"
                         "  -D             enable DEBUG output\n"
                         "  -V             prints version & exits\n"
                         "  --             pass rest of commandline to cdrecord\n"
                         "\n", prg_name);
                 exit(0);
       }
    }

  if(optind<argc) { /* save position/count of cdrecord options */
    cdrec_opt_count=argc-optind;
    cdrec_opt=&argv[optind];
    }
    
  if(cd_len<0) {
    auto_size=1;
    if(virtual && !virt_dump) serror("Can't auto-detect media size in virtual mode. Use option -l to set media size\n");
    }
  if(virtual && dvd && !virt_dump) {
     fprintf(stderr,"Option -R ignored in virtual mode\n");
     dvd=0;
     }
  if(dvd) {
    if(xamode2) fprintf(stderr,"Option -X ignored in DVD mode\n");
    padsize=0;
    }
  if(virt_dump && !virtual) serror("To dump an image you must supply the image name with -i\n");
  if(!cdr_dev && (!virtual || virt_dump)) serror("You must specify a device for cdrecord with -r\n");
}

/****************************************************************************/

void autosize(void)
{
  if(auto_size) {
    cd_len=atip_cdrecord();
    if(cd_len<0) serror("Media size detection failed. Use option -l to set media size\n");
    }
}

/****************************************************************************/

void dump(void)
{
  int n, cont;
  char buffer[CD_FRAMESIZE];
  long long grandTotal;

  do {
    int change;
    do {
      autosize();
      virtual=1;
      Vopen(1); n=VreadToc(0);
      if(n<1) serror("It's not usefull to dump an empty image");
      secs=Vsize(); cont=VhasCont();
      if(cd_len<secs) serror("Image doesn't fit to this media");
      Vseek(-1); VprepareDump();

      virtual=0; change=0;
      Vopen(0); n=VreadToc(0); VprintSpace();
      if(n!=0) {
        fprintf(stderr,"Can't dump to non-empty disk! Try another disk\n");
        change=1;
        }

      if(change) {
        Vclose();
        diskchange(multicmd,cd_dev);
        }
      } while(change);

    if(verbose)
      fprintf(stderr,"%s: Dumping image (%d blocks) to %s\n",prg_name,secs,VdevName());
    VnewTrack();

    grandTotal=0;
    while(secs>0) {
      VvirtRead(buffer);
      Vwrite(buffer); grandTotal+=CD_FRAMESIZE;
      secs--;
      }

    VcloseTrack(0);

    totalSize+=grandTotal;
    if(verbose) {
      char str1[16], str2[16];
      fprintf(stderr,"%s: Dumping finished. %s written (%s on this disk)\n",
              prg_name,FlexSize(str1,totalSize),FlexSize(str2,grandTotal));
      }

    if(multidisk==0) {
      if(cont) fprintf(stderr,"Multi-disk not enabled, ignoring continuation image(s)!\n");
      cont=0;
      }
    else if(cont) {
      disknum++;
      diskchange(multicmd,cd_dev);
      }
    } while(cont);
}

/****************************************************************************/

int backup(void)
{
  long long grandTotal=0;
  struct header_block header;
  int flags, datasize, result=0;

  char buffer[CD_FRAMESIZE];
  struct data_block *db=(struct data_block *)&buffer[0];

  flags=F_NONE;
  datasize=DATASIZE;
  if(crc) { flags|=F_CRC; datasize-=4; }

  sprintf(buffer,"%04d%02d%02d%02d%02d",curtime.tm_year+1900,
    curtime.tm_mon+1,curtime.tm_mday,curtime.tm_hour,curtime.tm_min);
  
  strncpy(header.id_str,HDR_STRING,32); header.id_str[32]=0;
  strncpy(header.vol_id,cd_label,32); header.vol_id[32]=0;
  strncpy(header.t_stamp,buffer,12); header.t_stamp[12]=0;
  header.disk_set = disknum;
  header.flags = flags;

  if(verbose)
    fprintf(stderr,"%s: Recording to %s, multidisk %s, CRC %s, disk %d\n",
            prg_name,VdevName(),
            multidisk?"enabled":"disabled",
            crc?"enabled":"disabled",
            disknum); 
  secs=cd_len;
  VnewTrack();

  memset(buffer,0,CD_FRAMESIZE);
  memcpy(buffer,&header,sizeof(struct header_block));
  Vwrite(buffer); grandTotal+=CD_FRAMESIZE;

  do {
    int bytes;

    db->flags=flags;
    db->status=0;		      /* this isn't the last block (for now) */
    bytes=full_read(0,&buffer[DBSIZE],datasize);
    if(bytes!=datasize) db->status=1; /* EOF, this is the last block */
    db->datasize=htons(bytes);

    if(cd_avail<(CD_FRAMESIZE*2)) {   /* less than 2 block free */
      if(db->status==0) {             /* if not last block, mark disk as full */
        db->status=2;
        result=1;
        }
      }
    if(crc) {
      int l=crc32(buffer,bytes+DBSIZE);
      *((unsigned long *)(&buffer[CD_FRAMESIZE-4]))=l;
      }
    Vwrite(buffer); grandTotal+=CD_FRAMESIZE;
    } while(db->status==0);

  if(dvd && cd_avail>=CD_FRAMESIZE) { /* pad up the track with zeros */
    memset(buffer,0,CD_FRAMESIZE);
    if(verbose) fprintf(stderr,"%s: padding up the track\n",prg_name);
    while(cd_avail>=CD_FRAMESIZE) Vwrite(buffer);
    }

  VcloseTrack(multidisk==0 ? 0 : result);

  totalSize+=grandTotal;
  if(verbose) {
    char str1[16], str2[16];
    fprintf(stderr,"%s: Recording finished. %s written (%s on this disk)\n",
            prg_name,FlexSize(str1,totalSize),FlexSize(str2,grandTotal));
    }
  return result;
}

/****************************************************************************/

int main(int argc, char *argv[]) 
{
  int result, loop;
  time_t curtime_t;

  curtime_t=time(0); curtime=*localtime(&curtime_t);
  parse_cmdline(argc,argv);

  if(virt_dump) {
    dump();
    }
  else {
    do {
      do {
        autosize();
        Vopen(0); result=VreadToc(0); VprintSpace();
        loop=1;

        if(disknum>1 && result!=0) {
          Vclose();
          fprintf(stderr,"%s: Can't do multidisk continuation on non-empty disk! Try another disk\n", prg_name);
          diskchange(multicmd,cd_dev);
          }
        else if(cd_avail<(padsize+MIN_BLOCKS)*CD_FRAMESIZE) {
          Vclose();
          if(multidisk) {
            fprintf(stderr,"%s: Not enough free space on disk! Try another disk\n", prg_name);
            diskchange(multicmd,cd_dev);
            }
          else serror("Not enough free space on disk");
          }
        else loop=0;
        } while(loop);

      result=backup();
      if(result==1) {
        if(multidisk==0) serror("Disk full, multi-disk not enabled. Aborting");

        disknum++;
        if(!VisRegular()) diskchange(multicmd,cd_dev);
        }
      } while(result!=0);
    }
  return 0;
}
