/* virtual.h
Copyright (c) 2000-2012 Craig Condit, Stefan Huelswitt.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#ifndef _CDBACKUP_VIRTUAL_H
#define _CDBACKUP_VIRTUAL_H

#define VIRT_VERSION    1
#define VIRT_MAGIC      0x144391C83A78412FLL
#define MAX_VIRT_TRACKS 96
#define VIRT_HEADER_LEN CD_FRAMESIZE

struct virt_header {
  long long magic;
  int version;
  int tracks;
  int leadout;
  int count, has_cont;
  int reserved[25];
  int start[MAX_VIRT_TRACKS];
  };

struct toc_entry {
  u_char track_no;	/* track number */
  char is_data;		/* 1 = data track */
  char is_cdbackup;	/* was it created by CD-Backup? */
  int sec_start;	/* start sector */
  int sec_end;		/* last sector */
  char id_str[33];	/* recorder id string (32 chars) */
  char vol_id[33];	/* volume label (32 characters) */
  char t_stamp[13];	/* time stamp: yyyymmddhhmm */
  char disk_set;	/* disk number */
  char flags;           /* backup flags */
  };

extern struct toc_entry *toc;
extern long long cd_used, cd_avail;

void Vopen(int ro);
void Vclose(void);
int VisRegular(void);
int VreadToc(int trackInfos);
void VprintSpace(void);
const char *VdevName(void);
long long Vseek(int trackNum);
void Vread(void *buf);
void VnewTrack(void);
void VcloseTrack(int cont);
void Vwrite(const void *buf);
void VgetAhead(void);
void VsetAhead(int restore);
int Vsize(void);
int VhasCont(void);
void VprepareDump(void);
void VvirtRead(void *buf);

#endif
